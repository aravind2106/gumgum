# GumGum Ad Events 

Parse the ad events log file and add next page Url for every event if applicable. 

## Getting Started

I have completed this assigment using Spark dataframe using scala and have used window function to carry out the same.  
I have grouped by every visitor ID and ordered the data with timestamp so that I can use the lead function to take the Url of the previous data.  
Null will be present if it is the last event for that vistor or the visitor is present in the log only once.  
I have filtered only not null values from the list. i.e., A valid log line is if it has data for all 5 columns(id, type, url, visitorid and timestamp).  

### Prerequisites

Spark, Scala and SBT (IntellijIDEA preferred)  
I have used coalesce(1) in my code. If this code is tested against millions or more events please remove that or change it to a different
NumPartition number as it will fail with Out of Memory error.  

### Installing

Install Scala, SBT (Version ID present in the code)  
Create a docker account  
Create a bitbucket account  

## Running the tests

sbt clean   
sbt clean compile assembly #this creates a JAR file  
spark-submit --class ParseAdEvents --master local target/scala-2.11/GumGumAdDataSet-assembly-0.1.0-SNAPSHOT.jar  

## Unit test

Tested the code with the following scenarios.  
1. File with unique Visitor  
2. File with 50k till more than 1 Million lines   
3. File with 1 visitorID for more than 50k add lines  
4. Missing Key in add lines of the Json Input  
5. Jumbled key value pairs  
6. Tested with Text Stream also (it worked, created my own Json parser.). My solution is a batch mode solution.  

## Deployment

Used docker to deploy this on a live system

docker build -t aravind2106/gumgum:latest -f ./Dockerfile .  
docker run aravind2106/gumgum:latest  
docker login  
docker push aravind2106/gumgum:latest  

## Built With

Scala and SBT and deployed the code into Docker and uploded the Docker image to a BitBucket repository.  


